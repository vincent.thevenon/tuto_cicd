package hangman;

import static org.junit.Assert.*;

import org.junit.Test;

public class testJUnit {

	@Test
	public void testModel() {
		Model model = new Model("Chocolate");
		assertEquals("CHOCOLATE", model.objective);
		assertEquals("---------", model.current);
		assertEquals("", model.errors);
	}

	@Test
	public void testTryLetter() {
		Model model = new Model("Chocolate");
		boolean b = model.tryLetter('o');
		assertTrue(b);
		assertEquals("--O-O----", model.current);
		assertEquals("", model.errors);
		b = model.tryLetter('c');
		assertTrue(b);
		assertEquals("C-OCO----", model.current);
		assertEquals("", model.errors);
	}

}